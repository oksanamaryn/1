// index.test.js
const { Builder, By, Key, until } = require('selenium-webdriver');
require('selenium-webdriver/chrome');
require('chromedriver');

//test update
const rootURL = 'https://www.mozilla.org/en-US/';
const waitUntilTime = 20000;
let driver, el, actual, expected;
//jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 *5;


describe ('suite', ()=> {

    beforeAll(async () => {
        driver = new Builder().forBrowser('chrome').build();
        await driver.get(rootURL);
    }, 10000);

    afterAll(async () => {
        await driver.quit();
    }, 15000);

    async function getElementByXpath(xpath) {
        const el = await driver.wait(until.elementLocated(By.xpath(xpath)), waitUntilTime);
        return await driver.wait(until.elementIsVisible(el), waitUntilTime);
    };


    it('should be click on navbar to display a drawer', async ()=> {
        el = await getElementByXpath('//*[@class="c-primary-cta-title"]')

        actual = await el.getText();
        expected = 'Take back your privacy';
        expect(actual).toEqual(expected);
    })
})
